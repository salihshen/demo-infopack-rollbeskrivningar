// load dependencies
var Handlebars = require('handlebars');
var fs = require('fs');

// prepare template
var source = fs.readFileSync('template.html', "utf8");
var template = Handlebars.compile(source);

// read data
var data = require('./data.json');

// populate template
var md = template({ data: data });

var dir = './content';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
// write to file
fs.writeFile(dir + "/index.md", md, function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("index.md created!");
    console.log('Infopack realisation Finished');
});
