var fs = require('fs');
var pandoc = require('node-pandoc');

var dir = 'deliverables';

if (!fs.existsSync('./' + dir)){
    fs.mkdirSync(dir);
}

// create docx
pandoc('./content/index.md', '-f markdown -t docx -o ./' + dir +'/dokument.docx', function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log('Infopack built without errors!');
	}
});

// create html
pandoc('./content/index.md', '-f markdown -t html -o ./' + dir +'/dokument.html', function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log('Infopack built without errors!');
	}
});

// create pdf
// pandoc('./content/index.md', '-f markdown -t latex -o ./' + dir +'/dokument.pdf', function(err) {
// 	if(err) {
// 		console.log(err);
// 	} else {
// 		console.log('Infopack built without errors!');
// 	}
// });
